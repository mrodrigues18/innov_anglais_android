import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:innov_anglais_android/fragments/account_pages/view_account.dart';
import 'package:innov_anglais_android/fragments/index_page/index_page.dart';
import 'package:innov_anglais_android/fragments/payment_pages/payment.dart';
import 'package:innov_anglais_android/fragments/test_pages/list_tests.dart';
import 'package:innov_anglais_android/modele/User.dart';
import 'admin_page.dart';

class DrawerItem {
  String title;
  IconData icon;
  DrawerItem(this.title, this.icon);
}

class HomePage extends StatefulWidget {
  final User currentUser;
  HomePage({Key? key, required this.currentUser}) : super(key: key);

  final drawerItemsAdministrateur = [
    DrawerItem("Accueil", Icons.home),
    DrawerItem("Tests", Icons.assignment),
    DrawerItem("Paiement", FontAwesomeIcons.stripe),
    DrawerItem("Administration", Icons.assignment),
    DrawerItem("Mon compte", Icons.person_outline),
    DrawerItem("Déconnexion", Icons.logout),
    DrawerItem("Version", Icons.info_outline_rounded),
  ];

  final drawerItemsUtilisateur = [
    DrawerItem("Accueil", Icons.home),
    DrawerItem("Tests", Icons.assignment),
    DrawerItem("Paiement", FontAwesomeIcons.stripe),
    DrawerItem("Mon compte", Icons.person_outline),
    DrawerItem("Déconnexion", Icons.logout),
    DrawerItem("Version", Icons.info_outline_rounded),
  ];

  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  int _selectedDrawerIndex = 0;

  _getDrawerItemWidget(int pos) {
    if(widget.currentUser.roleUser == 'Administrateur') {
      switch(pos) {
        case 0:
          return IndexPage(currentUser: widget.currentUser,);
        case 1:
          return TestListPage(currentUser: widget.currentUser,);
        case 2:
          return const PaymentPage();
        case 3:
          return const AdminListPage();
        case 4:
          return AccountViewPage(currentUser: widget.currentUser,);
        case 5:
          return Navigator.pop(context);
        case 6:
          return IndexPage(currentUser: widget.currentUser,);
        default:
          return const Center(child: Text("Une erreur s'est produite."),);
      }
    } else {
      switch(pos) {
        case 0:
          return IndexPage(currentUser: widget.currentUser,);
        case 1:
          return TestListPage(currentUser: widget.currentUser,);
        case 2:
          return const PaymentPage();
        case 3:
          return AccountViewPage(currentUser: widget.currentUser,);
        case 4:
          return Navigator.pop(context);
        case 5:
          return IndexPage(currentUser: widget.currentUser,);
        default:
          return const Center(child: Text("Une erreur s'est produite."),);
      }
    }
  }

  _onSelectItem(int index) {
    setState(() => _selectedDrawerIndex = index);
  }

  @override
  Widget build(BuildContext context) {
    var drawerOptions = <Widget>[];
    if(widget.currentUser.roleUser == 'Administrateur') {
      for (var i = 0; i < widget.drawerItemsAdministrateur.length; i++) {
        var d = widget.drawerItemsAdministrateur[i];
        drawerOptions.add(
            ListTile(
              leading: Icon(d.icon),
              title: Text(d.title),
              selected: i == _selectedDrawerIndex,
              onTap: () {
                // Permet de faire disparaître la SideBar après le cliqué sur l'un des menus
                Navigator.pop(context);
                _onSelectItem(i);
              },
            )
        );
      }

      return Scaffold(
        appBar: AppBar(
          title: Text('Innov\'Anglais - ' + widget.drawerItemsAdministrateur[_selectedDrawerIndex].title),
        ),
        drawer: ListView(
          children: <Widget>[
            Card(
              child: Column(
                children: <Widget>[
                  UserAccountsDrawerHeader(
                    accountName: Text(widget.currentUser.usernameUser),
                    accountEmail: Text(widget.currentUser.email),
                    currentAccountPicture: CircleAvatar(
                      backgroundColor: Colors.white,
                      child:
                      Image.network(
                        'http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/logo_innovanglais.png',
                        height: 100.0,
                        width: 100.0,
                      ),
                    ),
                  ),
                  Column(
                    children: drawerOptions,
                  ),
                ],
              ),
            )
          ],
        ),
        body: _getDrawerItemWidget(_selectedDrawerIndex),
      );
    } else {
      for (var i = 0; i < widget.drawerItemsUtilisateur.length; i++) {
        var d = widget.drawerItemsUtilisateur[i];
        drawerOptions.add(
            ListTile(
              leading: Icon(d.icon),
              title: Text(d.title),
              selected: i == _selectedDrawerIndex,
              onTap: () {
                // Permet de faire disparaître la SideBar après le cliqué sur l'un des menus
                Navigator.pop(context);
                _onSelectItem(i);
              },
            ),
        );
      }
      return Scaffold(
        appBar: AppBar(
          title: Text('Innov\'Anglais - ' + widget.drawerItemsUtilisateur[_selectedDrawerIndex].title),
        ),
        drawer: ListView(
          children: <Widget>[
            Card(
              child: Column(
                children: <Widget>[
                  UserAccountsDrawerHeader(
                    accountName: Text(widget.currentUser.usernameUser),
                    accountEmail: Text(widget.currentUser.email),
                    currentAccountPicture: CircleAvatar(
                      backgroundColor: Colors.white,
                      child: Image.network(
                        'http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/logo_innovanglais.png',
                        height: 100.0,
                        width: 100.0,
                      ),
                    ),
                    onDetailsPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  Column(
                    children: drawerOptions,
                  ),
                ],
              ),
            )
          ],
        ),
        body: _getDrawerItemWidget(_selectedDrawerIndex),
      );
    }
  }
}