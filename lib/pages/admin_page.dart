import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:innov_anglais_android/fragments/category_pages/list_category.dart';
import 'package:innov_anglais_android/fragments/firm_pages/list_firm.dart';
import 'package:innov_anglais_android/fragments/level_pages/list_level.dart';
import 'package:innov_anglais_android/fragments/sub_pages/list_sub.dart';
import 'package:innov_anglais_android/fragments/theme_pages/list_theme.dart';
import 'package:innov_anglais_android/fragments/user_pages/list_user.dart';

class AdminListPage extends StatefulWidget {
  const AdminListPage({Key? key}) : super(key: key);

  @override
  _AdminListPageState createState() => _AdminListPageState();
}

class _AdminListPageState extends State<AdminListPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Row(
            //ROW 1
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: 175,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => const CategorieListPage())
                    );
                  },
                  child: const Text(
                      'Catégorie',
                      style: TextStyle(fontSize: 20)
                  ),
                ),
              ),
              SizedBox(
                width: 175,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => const FirmListPage())
                    );
                  },
                  child: const Text(
                      'Entreprise',
                      style: TextStyle(fontSize: 20)
                  ),
                ),
              ),
            ],
          ),
          Row(
            //ROW 2
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  width: 175,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => const LevelListPage())
                      );
                    },
                    child: const Text(
                        'Niveau',
                        style: TextStyle(fontSize: 20)
                    ),
                  ),
                ),
                SizedBox(
                  width: 175,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => const SubscriptionListPage()));
                    },
                    child: const Text(
                        'Abonnement',
                        style: TextStyle(fontSize: 20)
                    ),
                  ),
                ),
              ]),
          Row(
            // ROW 3
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: 175,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => const UserListPage())
                    );
                  },
                  child: const Text(
                      'Utilisateur',
                      style: TextStyle(fontSize: 20)
                  ),
                ),
              ),
              SizedBox(
                width: 175,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => const ThemeeListPage())
                    );
                  },
                  child: const Text(
                      'Thème',
                      style: TextStyle(fontSize: 20)
                  ),
                ),
              ),
            ],
          ),
        ]
    );
  }
}



