import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:innov_anglais_android/modele/Firm.dart';
import 'package:shimmer/shimmer.dart';

class FirmListPage extends StatefulWidget {
  const FirmListPage({Key? key}) : super(key: key);

  @override
  _FirmListPageState createState() => _FirmListPageState();
}

class _FirmListPageState extends State<FirmListPage> {
  final _formFirm = GlobalKey<FormState>();
  TextEditingController textEditingControllerFirmLabel = TextEditingController();
  bool showInputController = false;

  final Uri url = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/firms_JSON");

  Future<List<Firm>> fetchFirms() async {
    var response = await http.get(url);

    if (response.statusCode == 200) {
      final items = json.decode(response.body).cast<Map<String, dynamic>>();
      List<Firm> listOfFirms = items.map<Firm>((json) {
        return Firm.fromJson(json);
      }).toList();

      return listOfFirms;
    } else {
      throw Exception('Une erreur s\'est produite.');
    }
  }

  void main() {
    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.dumpErrorToConsole(details);
      if (kReleaseMode) exit(1);
    };
  }

  bool _validationFirm() {
    return !(textEditingControllerFirmLabel.text != "");
  }

  insertFirm(String nom) async {
    final Uri url = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/insert_firm_android/$nom");

    var response = await http.get(url);

    if(response.statusCode == 200) {
      if(response.body.contains("success")) ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Donnée enregistrée'), backgroundColor: Colors.lightGreenAccent,));
      if(response.body.contains("error")) ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Une erreur s\'est produite'), backgroundColor: Colors.deepOrange,));

      /// Actualise la liste et vide les champs
      setState(() {
        textEditingControllerFirmLabel.text = "";
        showInputController = !showInputController;
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Une erreur s\'est produite: error ${response.statusCode}'), backgroundColor: Colors.deepOrange,));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(!showInputController ? 'Innov\'Anglais - Entreprises' : 'Innov\'Anglais - Ajouter Entreprise'),
        automaticallyImplyLeading: !showInputController ? true : false,
        actions: <Widget>[
          Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    textEditingControllerFirmLabel.text = "";
                    showInputController = !showInputController;
                  });
                },
                child: Icon(
                    !showInputController ? Icons.add_circle : Icons.remove_circle_outlined
                ),
              )
          ),
        ],
      ),
      body: Center(
        child: FutureBuilder<List<Firm>>(
          future: fetchFirms(),
          builder: (context, snapshot) {
            /// Affiche un squelette tant que les données ne sont pas récuperées
            if (!snapshot.hasData) {
              return ListView(
                children: <Widget>[
                  Center(
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey[300]!,
                      highlightColor: Colors.grey[100]!,
                      child: Column(
                        children: <int>[0, 1, 2, 3, 4, 5, 6, 7].map((_) => Padding(
                          padding: const EdgeInsets.only(
                              bottom: 10.0,
                              top: 10.0,
                              right: 10.0,
                              left: 10.0
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 48.0,
                                height: 48.0,
                                color: Colors.white,
                              ),
                              const Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8.0),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: double.infinity,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 2.0),
                                    ),
                                    Container(
                                      width: double.infinity,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 2.0),
                                    ),
                                    Container(
                                      width: 40.0,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )).toList(),
                      ),
                    ),
                  )
                ],
              );
            }

            /// Si table vide dans la BDD
            if (snapshot.data!.isEmpty) {
              return const Center(
                child: Text("Aucun firm"),
              );
            }
                  ///Si l'on récupère les données
            return showInputController ?
            Form(
                key: _formFirm,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: SizedBox(
                          width: 200.0,
                          child: TextFormField(
                              controller: textEditingControllerFirmLabel,
                              maxLines: 1,
                              onChanged: (value) {
                                setState(() {
                                  _validationFirm();
                                });
                              },
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              textCapitalization: TextCapitalization.sentences,
                              decoration: const InputDecoration(
                                labelText: "Ajouter une entreprise...",
                                hintText: "Ajouter une entreprise...",
                                border: OutlineInputBorder(),
                              )
                          )
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: SizedBox(
                          width: 200.0,
                          child: ElevatedButton(
                            child: Text("Ajouter l'entreprise !", style: TextStyle(color: _validationFirm() ? Colors.white10 : null),),
                            onPressed: () {
                              if(textEditingControllerFirmLabel.text != "") {
                                if(_formFirm.currentState!.validate()){
                                  insertFirm(textEditingControllerFirmLabel.text);
                                }
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Veuillez compléter le champ'), backgroundColor: Colors.redAccent,));
                                return;
                              }
                            },
                          )
                      ),
                    )
                  ],
                )
            ) : ListView(
              children: snapshot.data!.map((data) => Card(
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      CupertinoButton(
                        child: ListTile(
                          leading: Text(
                            data.nameFirm,
                            style: const TextStyle(
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        onPressed: () {

                        },
                      ),
                    ],
                  ),
                ),
              )).toList(),
            );
          },
        ),
      ),
    );
  }
}
