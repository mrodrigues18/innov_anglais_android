import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:innov_anglais_android/modele/User.dart';
import 'package:translator/translator.dart';

class IndexPage extends StatefulWidget {
  final User currentUser;
  const IndexPage({Key? key, required this.currentUser}) : super(key: key);

  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {

  GoogleTranslator translator = GoogleTranslator();

  var welcomeLabelText;
  var labelText;

  @override
  void initState() {
    super.initState();
    translate();
  }

  void translate() async {
    await translator
        .translate("Bienvenue", to: widget.currentUser.language_user)
        .then((value) {
      setState(() {
        welcomeLabelText = value.text;
      });
    });
    await translator
        .translate("Sur InnovAnglais vous retrouverez,\ndes tests pour améliorer votre anglais.", to: widget.currentUser.language_user)
        .then((value) {
      setState(() {
        labelText = value.text;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SvgPicture.network(
                'http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/vecteur_cours_en_ligne.svg',
                height: 200.0,
                width: 500.0,
              ),
              Row(
                //ROW 1
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 50.0, 0, 50.0),
                      child: Text(welcomeLabelText,
                          style: const TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold
                          )
                      ),
                    ),
                  ),
                ]),
              Row(
                //ROW 2
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0,20,0,0),
                        child: Text(labelText,
                            maxLines: 20,
                            textAlign: TextAlign.justify,
                            style: const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            )
                        ),
                      ),
                    ),
                  ]
              ),
            ]
        ),
    );
  }
}
