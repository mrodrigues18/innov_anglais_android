import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:innov_anglais_android/fragments/test_pages/view_test.dart';
import 'package:innov_anglais_android/modele/Test.dart';
import 'package:shimmer/shimmer.dart';

class TestListPage extends StatefulWidget {
  final currentUser;
  const TestListPage({Key? key, required this.currentUser}) : super(key: key);

  @override
  _TestListPageState createState() => _TestListPageState();
}

class _TestListPageState extends State<TestListPage> {
  final Uri url = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/test_JSON");

  Future<List<TestDo>> fetchTests() async {
    var response = await http.get(url);

    if (response.statusCode == 200) {
      final items = json.decode(response.body).cast<Map<String, dynamic>>();
      List<TestDo> listOfTests = items.map<TestDo>((json) {
        return TestDo.fromJson(json);
      }).toList();

      return listOfTests;
    } else {
      throw Exception('Une erreur s\'est produite.');
    }
  }

  void main() {
    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.dumpErrorToConsole(details);
      if (kReleaseMode) exit(1);
    };
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FutureBuilder<List<TestDo>>(
          future: fetchTests(),
          builder: (context, snapshot) {
            /// Affiche un squelette tant que les données ne sont pas récuperées
            if (!snapshot.hasData) {
              return ListView(
                children: <Widget>[
                  Center(
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey[300]!,
                      highlightColor: Colors.grey[100]!,
                      child: Column(
                        children: <int>[0, 1, 2, 3, 4, 5, 6, 7].map((_) => Padding(
                          padding: const EdgeInsets.only(
                              bottom: 10.0,
                              top: 10.0,
                              right: 10.0,
                              left: 10.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 48.0,
                                height: 48.0,
                                color: Colors.white,
                              ),
                              const Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8.0),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: double.infinity,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 2.0),
                                    ),
                                    Container(
                                      width: double.infinity,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 2.0),
                                    ),
                                    Container(
                                      width: 40.0,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )).toList(),
                      ),
                    ),
                  )
                ],
              );
            }

            /// Si table vide dans la BDD
            if (snapshot.data!.isEmpty) {
              return const Center(
                child: Text("Aucun test"),
              );
            }

            return ListView(
              children: snapshot.data!.map((data) => Card(
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      CupertinoButton(
                        child: ListTile(
                          leading: Text(
                            data.testLabel,
                            style: const TextStyle(
                                fontWeight: FontWeight.bold
                            ),
                          ),
                          title: Text("Thème : " + data.themeLabel +
                              '\n' + "Niveau : " +
                              data.levelLabel, textAlign: TextAlign.end,),
                        ),
                        onPressed: () {
                          Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) {
                                return TestViewPage(themeLabel: data.themeLabel, levelLabel: data.levelLabel, testLabel: data.testLabel, testId: data.testId, userId: widget.currentUser.idUser, currentUser: widget.currentUser);
                              })
                          );
                        },
                      ),
                    ],
                  ),
                ),
              )).toList(),
            );
          },
        ),
      ),
    );
  }
}
