import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:innov_anglais_android/modele/Test.dart';
import 'package:innov_anglais_android/modele/User.dart';
import 'package:shimmer/shimmer.dart';

class TestViewPage extends StatefulWidget {
  final String themeLabel;
  final String levelLabel;
  final String testLabel;
  final String testId;
  final String userId;
  final User   currentUser;

  const TestViewPage({Key? key, required this.themeLabel, required this.levelLabel, required this.testLabel, required this.testId, required this.userId, required this.currentUser}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _TestViewPageState();
  }
}

class _TestViewPageState extends State<TestViewPage> {
  final _numberFormA = GlobalKey<FormState>();
  final _numberFormB = GlobalKey<FormState>();
  final _numberFormC = GlobalKey<FormState>();
  TextEditingController textController1 = TextEditingController();
  TextEditingController textController2 = TextEditingController();
  TextEditingController textController3 = TextEditingController();
  TextEditingController textController4 = TextEditingController();
  String answer1 = "";
  String answer2 = "";
  String answer3 = "";
  String answer4 = "";
  RegExp stringValidator  = RegExp(r'^[a-zA-Z]+$');
  bool isAString1 = false;
  bool isAString2 = false;
  bool isAString3 = false;
  bool isAString4 = false;
  bool isValidForm = false;
  bool isSelected1 = false;
  bool isSelected1A = false;
  bool isSelected1B = false;
  bool isSelected1C = false;
  bool isSelected2A = false;
  bool isSelected2B = false;
  bool isSelected2C = false;
  bool isSelected3A = false;
  bool isSelected3B = false;
  bool isSelected3C = false;
  bool isSelected4A = false;
  bool isSelected4B = false;
  bool isSelected4C = false;
  bool isSelected3 = false;
  bool isSelected4 = false;
  Widget questions = const Text("Une erreur s'est produite");
  int nbQuestion = 4;
  int _counter = 60;
  late Timer _timer;

  Future<List<Test>> fetchQuestions() async {
    final Uri url = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/test_do_android/${widget.themeLabel},${widget.levelLabel},${widget.testLabel},${widget.testId},${widget.userId}");

    var response = await http.get(url);

    if(response.statusCode == 200) {
      final items = json.decode(response.body).cast<Map<String, dynamic>>();
      List<Test> listOfQuestions = items.map<Test>((json) {
        return Test.fromJson(json);
      }).toList();

      return listOfQuestions;
    } else {
      throw Exception("Une erreur s'est produite lors de la récupération des questions");
    }
  }

  @override
  initState() {
    super.initState();
    _startTimer();
  }

  void _startTimer() {
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_counter > 0) {
        setState(() {
          _counter--;
        });
      } else {
        _timer.cancel();
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Perdu, veuillez réessayer !'), backgroundColor: Colors.redAccent,));
        Navigator.pop(context);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    setState(() {
      _timer.cancel();
      _counter = 60;
    });
  }

  void main() {
    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.dumpErrorToConsole(details);
      if (kReleaseMode) exit(1);
    };
  }

  void setValidator1(valid){
    setState(() {
      isAString1 = valid;
    });
  }
  void setValidator2(valid){
    setState(() {
      isAString2 = valid;
    });
  }
  void setValidator3(valid){
    setState(() {
      isAString3 = valid;
    });
  }
  void setValidator4(valid){
    setState(() {
      isAString4 = valid;
    });
  }

  void gestionAffichage(int question) {
    switch(question) {
      case 1:
        isSelected1A = false;
        isSelected1B = false;
        isSelected1C = false;
        break;
      case 2:
        isSelected2A = false;
        isSelected2B = false;
        isSelected2C = false;
        break;
      case 3:
        isSelected3A = false;
        isSelected3B = false;
        isSelected3C = false;
        break;
      case 4:
        isSelected4A = false;
        isSelected4B = false;
        isSelected4C = false;
        break;
    }
    _validation();
    _validationC();
  }

  bool _validation() {
    return !(answer1 != "" && answer2 != "" && answer3 != "" && answer4 != "");
  }
  bool _validationC() {
    return !(textController1.text != "" && textController2.text != "" && textController3.text != "" && textController4.text != "");
  }

  Widget question(BuildContext context, Test data, String levelLabel) {
    if(levelLabel == "A0" || levelLabel == "A1" || levelLabel == "A1") {
      for(var i = 1 ; i <= nbQuestion ; i++) {
        questions = Form(
            key: _numberFormA,
            child: Column(
              children: [
                Center(
                    child: Padding(
                        padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                  children: [
                                    const Text("Question 1", style: TextStyle(fontSize: 35.0)),
                                    Text(data.question1Label, style: const TextStyle(fontSize: 15.0)),
                                    Row(
                                        children: [
                                          OutlinedButton(
                                              style: ButtonStyle(
                                                backgroundColor: isSelected1A ? MaterialStateProperty.all<Color>(Colors.white) : MaterialStateProperty.all<Color>(Colors.transparent),
                                              ),
                                              child: Text(data.question1Answer),
                                              onPressed: () {
                                                gestionAffichage(1);
                                                setState(() {
                                                  isSelected1A = true;
                                                  answer1 = data.question1Answer;
                                                });
                                              }
                                          ),
                                          OutlinedButton(
                                              style: ButtonStyle(
                                                backgroundColor: isSelected1B ? MaterialStateProperty.all<Color>(Colors.white) : MaterialStateProperty.all<Color>(Colors.transparent),
                                              ),
                                              child: Text(data.question1FakeWords1),
                                              onPressed: () {
                                                gestionAffichage(1);
                                                setState(() {
                                                  isSelected1B = true;
                                                  answer1 = data.question1FakeWords1;
                                                });
                                              }
                                          ),
                                          OutlinedButton(
                                              style: ButtonStyle(
                                                backgroundColor: isSelected1C ? MaterialStateProperty.all<Color>(Colors.white) : MaterialStateProperty.all<Color>(Colors.transparent),
                                              ),
                                              child: Text(data.question1FakeWords2),
                                              onPressed: () {
                                                gestionAffichage(1);
                                                setState(() {
                                                  isSelected1C = true;
                                                  answer1 = data.question1FakeWords2;
                                                });
                                              }
                                          )
                                        ]
                                    )
                                  ]
                              ),
                            ]
                        )
                    )
                ),
                Center(
                    child: Padding(
                        padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                  children: [
                                    const Text("Question 2", style: TextStyle(fontSize: 35.0)),
                                    Text(data.question2Label, style: const TextStyle(fontSize: 15.0,)),
                                    Row(
                                        children: [
                                          OutlinedButton(
                                              style: ButtonStyle(
                                                backgroundColor: isSelected2A ? MaterialStateProperty.all<Color>(Colors.white) : MaterialStateProperty.all<Color>(Colors.transparent),
                                              ),
                                              child: Text(data.question2Answer),
                                              onPressed: () {
                                                gestionAffichage(2);
                                                setState(() {
                                                  isSelected2A = true;
                                                  answer2 = data.question2Answer;
                                                });
                                              }
                                          ),
                                          OutlinedButton(
                                              style: ButtonStyle(
                                                backgroundColor: isSelected2B ? MaterialStateProperty.all<Color>(Colors.white) : MaterialStateProperty.all<Color>(Colors.transparent),
                                              ),
                                              child: Text(data.question2FakeWords1),
                                              onPressed: () {
                                                gestionAffichage(2);
                                                setState(() {
                                                  isSelected2B = true;
                                                  answer2 = data.question2FakeWords1;
                                                });
                                              }
                                          ),
                                          OutlinedButton(
                                              style: ButtonStyle(
                                                backgroundColor: isSelected2C ? MaterialStateProperty.all<Color>(Colors.white) : MaterialStateProperty.all<Color>(Colors.transparent),
                                              ),
                                              child: Text(data.question2FakeWords2),
                                              onPressed: () {
                                                gestionAffichage(2);
                                                setState(() {
                                                  isSelected2C = true;
                                                  answer2 = data.question2FakeWords2;
                                                });
                                              }
                                          )
                                        ]
                                    )
                                  ]
                              ),
                            ]
                        )
                    )
                ),
                Center(
                    child: Padding(
                        padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                  children: [
                                    const Text("Question 3", style: TextStyle(fontSize: 35.0)),
                                    Text(data.question3Label, style: const TextStyle(fontSize: 15.0)),
                                    Row(
                                        children: [
                                          OutlinedButton(
                                              style: ButtonStyle(
                                                backgroundColor: isSelected3A ? MaterialStateProperty.all<Color>(Colors.white) : MaterialStateProperty.all<Color>(Colors.transparent),
                                              ),
                                              child: Text(data.question3Answer),
                                              onPressed: () {
                                                gestionAffichage(3);
                                                setState(() {
                                                  isSelected3A = true;
                                                  answer3 = data.question3Answer;
                                                });
                                              }
                                          ),
                                          OutlinedButton(
                                              style: ButtonStyle(
                                                backgroundColor: isSelected3B ? MaterialStateProperty.all<Color>(Colors.white) : MaterialStateProperty.all<Color>(Colors.transparent),
                                              ),
                                              child: Text(data.question3FakeWords1),
                                              onPressed: () {
                                                gestionAffichage(3);
                                                setState(() {
                                                  isSelected3B = true;
                                                  answer3 = data.question3FakeWords1;
                                                });
                                              }
                                          ),
                                          OutlinedButton(
                                              style: ButtonStyle(
                                                backgroundColor: isSelected3C ? MaterialStateProperty.all<Color>(Colors.white) : MaterialStateProperty.all<Color>(Colors.transparent),
                                              ),
                                              child: Text(data.question3FakeWords2),
                                              onPressed: () {
                                                gestionAffichage(3);
                                                setState(() {
                                                  isSelected3C = true;
                                                  answer3 = data.question3FakeWords2;
                                                });
                                              }
                                          )
                                        ]
                                    )
                                  ]
                              ),
                            ]
                        )
                    )
                ),
                Center(
                    child: Padding(
                        padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                  children: [
                                    const Text("Question 4", style: TextStyle(fontSize: 35.0)),
                                    Text(data.question4Label, style: const TextStyle(fontSize: 15.0)),
                                    Row(
                                        children: [
                                          OutlinedButton(
                                              style: ButtonStyle(
                                                backgroundColor: isSelected4A ? MaterialStateProperty.all<Color>(Colors.white) : MaterialStateProperty.all<Color>(Colors.transparent),
                                              ),
                                              child: Text(data.question4Answer),
                                              onPressed: () {
                                                gestionAffichage(4);
                                                setState(() {
                                                  isSelected4A = true;
                                                  answer4 = data.question4Answer;
                                                });
                                              }
                                          ),
                                          OutlinedButton(
                                              style: ButtonStyle(
                                                backgroundColor: isSelected4B ? MaterialStateProperty.all<Color>(Colors.white) : MaterialStateProperty.all<Color>(Colors.transparent),
                                              ),
                                              child: Text(data.question4FakeWords1),
                                              onPressed: () {
                                                gestionAffichage(4);
                                                setState(() {
                                                  isSelected4B = true;
                                                  answer4 = data.question4FakeWords1;
                                                });
                                              }
                                          ),
                                          OutlinedButton(
                                              style: ButtonStyle(
                                                backgroundColor: isSelected4C ? MaterialStateProperty.all<Color>(Colors.white) : MaterialStateProperty.all<Color>(Colors.transparent),
                                              ),
                                              child: Text(data.question4FakeWords2),
                                              onPressed: () {
                                                gestionAffichage(4);
                                                setState(() {
                                                  isSelected4C = true;
                                                  answer4 = data.question4FakeWords2;
                                                });
                                              }
                                          )
                                        ]
                                    )
                                  ]
                              ),
                            ]
                        )
                    )
                ),
                Center(
                  child: ElevatedButton(
                    child: Text("Valider mes réponses", style: TextStyle(color: _validation() ? Colors.white10 : null)),
                    onPressed: () {
                      if(answer1 != "" && answer2 != "" && answer3 != "" && answer4 != "") {
                        if(_numberFormA.currentState!.validate()){
                          setState(() {
                            isValidForm = true;
                          });
                          updateTest(answer1, answer2, answer3, answer4, data);
                        }
                      } else {
                        setState(() {
                          isValidForm = false;
                        });
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Veuillez remplir tous les champs'), backgroundColor: Colors.redAccent,));
                        return;
                      }
                    },
                  ),
                )
              ],
            )
        );
      }
    } else if(levelLabel == "B1" || levelLabel == "B2") {
      for(var i = 1 ; i <= nbQuestion ; i++) {
        questions = Form(
            key: _numberFormB,
            child: Column(
              children: [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          children: [
                            const Text("Question 1", style: TextStyle(fontSize: 35.0)),
                            Text(data.question1Label, style: const TextStyle(fontSize: 15.0)),
                            Row(
                              children: [
                                GestureDetector(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.network('http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/quiz/${widget.themeLabel}/${widget.testLabel}/${data.question1Answer}.png',
                                        width: 110.0, height: 110.0
                                    ),
                                  ),
                                  onLongPress: () {
                                    gestionAffichage(1);
                                    setState(() {
                                      isSelected1A = true;
                                      answer1 = data.question1Answer;
                                    });
                                  },
                                ),
                                GestureDetector(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.network('http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/quiz/${widget.themeLabel}/${widget.testLabel}/${data.question1FakeWords1}.png',
                                        width: 110.0, height: 110.0
                                    ),
                                  ),
                                  onLongPress: () {
                                    gestionAffichage(1);
                                    setState(() {
                                      isSelected1B = true;
                                      answer1 = data.question1FakeWords2;
                                    });
                                  },
                                ),
                                GestureDetector(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.network('http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/quiz/${widget.themeLabel}/${widget.testLabel}/${data.question1FakeWords2}.png',
                                        width: 110.0, height: 110.0
                                    ),
                                  ),
                                  onLongPress: () {
                                    gestionAffichage(1);
                                    setState(() {
                                      isSelected1C = true;
                                      answer1 = data.question1FakeWords2;
                                    });
                                  },
                                ),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          children: [
                            const Text("Question 2", style: TextStyle(fontSize: 35.0)),
                            Text(data.question2Label, style: const TextStyle(fontSize: 15.0)),
                            Row(
                              children: [
                                GestureDetector(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.network('http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/quiz/${widget.themeLabel}/${widget.testLabel}/${data.question2Answer}.png',
                                        width: 110.0, height: 110.0
                                    ),
                                  ),
                                  onLongPress: () {
                                    gestionAffichage(2);
                                    setState(() {
                                      isSelected2A = true;
                                      answer2 = data.question2Answer;
                                    });
                                  },
                                ),
                                GestureDetector(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.network('http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/quiz/${widget.themeLabel}/${widget.testLabel}/${data.question2FakeWords1}.png',
                                        width: 110.0, height: 110.0
                                    ),
                                  ),
                                  onLongPress: () {
                                    gestionAffichage(2);
                                    setState(() {
                                      isSelected2B = true;
                                      answer2 = data.question2FakeWords1;
                                    });
                                  },
                                ),
                                GestureDetector(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.network('http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/quiz/${widget.themeLabel}/${widget.testLabel}/${data.question2FakeWords2}.png',
                                        width: 110.0, height: 110.0
                                    ),
                                  ),
                                  onLongPress: () {
                                    gestionAffichage(2);
                                    setState(() {
                                      isSelected2C = true;
                                      answer2 = data.question2FakeWords2;
                                    });
                                  },
                                ),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          children: [
                            const Text("Question 3", style: TextStyle(fontSize: 35.0)),
                            Text(data.question3Label, style: const TextStyle(fontSize: 15.0)),
                            Row(
                              children: [
                                GestureDetector(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.network('http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/quiz/${widget.themeLabel}/${widget.testLabel}/${data.question3Answer}.png',
                                        width: 110.0, height: 110.0
                                    ),
                                  ),
                                  onLongPress: () {
                                    gestionAffichage(3);
                                    setState(() {
                                      isSelected3A = true;
                                      answer3 = data.question3Answer;
                                    });
                                  },
                                ),
                                GestureDetector(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.network('http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/quiz/${widget.themeLabel}/${widget.testLabel}/${data.question3FakeWords1}.png',
                                        width: 110.0, height: 110.0
                                    ),
                                  ),
                                  onLongPress: () {
                                    gestionAffichage(3);
                                    setState(() {
                                      isSelected3B = true;
                                      answer3 = data.question3FakeWords1;
                                    });
                                  },
                                ),
                                GestureDetector(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.network('http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/quiz/${widget.themeLabel}/${widget.testLabel}/${data.question3FakeWords2}.png',
                                        width: 110.0, height: 110.0
                                    ),
                                  ),
                                  onLongPress: () {
                                    gestionAffichage(3);
                                    setState(() {
                                      isSelected3C = true;
                                      answer3 = data.question3FakeWords2;
                                    });
                                  },
                                ),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          children: [
                            const Text("Question 4", style: TextStyle(fontSize: 35.0)),
                            Text(data.question4Label, style: const TextStyle(fontSize: 15.0)),
                            Row(
                              children: [
                                GestureDetector(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.network('http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/quiz/${widget.themeLabel}/${widget.testLabel}/${data.question4Answer}.png',
                                        width: 110.0, height: 110.0
                                    ),
                                  ),
                                  onLongPress: () {
                                    gestionAffichage(4);
                                    setState(() {
                                      isSelected4A = true;
                                      answer4 = data.question4Answer;
                                    });
                                  },
                                ),
                                GestureDetector(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.network('http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/quiz/${widget.themeLabel}/${widget.testLabel}/${data.question4FakeWords1}.png',
                                        width: 110.0, height: 110.0
                                    ),
                                  ),
                                  onLongPress: () {
                                    gestionAffichage(4);
                                    setState(() {
                                      isSelected4B = true;
                                      answer4 = data.question4FakeWords1;
                                    });
                                  },
                                ),
                                GestureDetector(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.network('http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/quiz/${widget.themeLabel}/${widget.testLabel}/${data.question4FakeWords2}.png',
                                        width: 110.0, height: 110.0
                                    ),
                                  ),
                                  onLongPress: () {
                                    gestionAffichage(4);
                                    setState(() {
                                      isSelected4C = true;
                                      answer4 = data.question4FakeWords2;
                                    });
                                  },
                                ),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Center(
                  child: ElevatedButton(
                    child: Text("Valider mes réponses", style: TextStyle(color: _validation() ? Colors.white10 : null)),
                    onPressed: () {
                      if(answer1 != "" && answer2 != "" && answer3 != "" && answer4 != "") {
                        if(_numberFormB.currentState!.validate()){
                          setState(() {
                            isValidForm = true;
                          });
                          updateTest(answer1, answer2, answer3, answer4, data);
                        }
                      } else {
                        setState(() {
                          isValidForm = false;
                        });
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Veuillez remplir tous les champs'), backgroundColor: Colors.redAccent,));
                        return;
                      }
                    },
                  ),
                )
              ],
            )
        );
      }
    } else if(levelLabel == "C1" || levelLabel == "C2") {
      for(var i = 1 ; i <= nbQuestion ; i++) {
        questions = Form(
            key: _numberFormC,
            child: Column(
              children: [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          children: [
                            const Text("Question 1", style: TextStyle(fontSize: 35.0)),
                            Text(data.question1Label, style: const TextStyle(fontSize: 15.0)),
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                                  child: SizedBox(
                                      width: 200.0,
                                      child: TextFormField(
                                          validator: (inputValue1) {
                                            if(!stringValidator.hasMatch(inputValue1!)){
                                              return "Lettres uniquement";
                                            }
                                            return null;
                                          },
                                          onChanged: (inputValue1) {
                                            if(stringValidator.hasMatch(inputValue1)){
                                              setValidator1(true);
                                            } else {
                                              setValidator1(false);
                                            }
                                          },
                                          controller: textController1,
                                          maxLines: 1,
                                          keyboardType: TextInputType.text,
                                          textInputAction: TextInputAction.next,
                                          textCapitalization: TextCapitalization.sentences,
                                          decoration: InputDecoration(
                                              labelText: "Votre proposition...",
                                              hintText: "Votre proposition...",
                                              border: const OutlineInputBorder(),
                                              errorText: isAString1 ? null : "Lettres uniquement"
                                          )
                                      )
                                  ),
                                ),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          children: [
                            const Text("Question 2", style: TextStyle(fontSize: 35.0)),
                            Text(data.question2Label, style: const TextStyle(fontSize: 15.0)),
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                                  child: SizedBox(
                                      width: 200.0,
                                      child: TextFormField(
                                          validator: (inputValue2) {
                                            if(!stringValidator.hasMatch(inputValue2!)){
                                              return "Lettres uniquement";
                                            }
                                            return null;
                                          },
                                          onChanged: (inputValue2) {
                                            if(stringValidator.hasMatch(inputValue2)){
                                              setValidator2(true);
                                            } else {
                                              setValidator2(false);
                                            }
                                          },
                                          controller: textController2,
                                          maxLines: 1,
                                          keyboardType: TextInputType.text,
                                          textInputAction: TextInputAction.next,
                                          textCapitalization: TextCapitalization.sentences,
                                          decoration: InputDecoration(
                                              labelText: "Votre proposition...",
                                              hintText: "Votre proposition...",
                                              border: const OutlineInputBorder(),
                                              errorText: isAString2 ? null : "Lettres uniquement"
                                          )
                                      )
                                  ),
                                ),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          children: [
                            const Text("Question 3", style: TextStyle(fontSize: 35.0)),
                            Text(data.question3Label, style: const TextStyle(fontSize: 15.0)),
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                                  child: SizedBox(
                                      width: 200.0,
                                      child: TextFormField(
                                          validator: (inputValue3) {
                                            if(!stringValidator.hasMatch(inputValue3!)){
                                              return "Lettres uniquement";
                                            }
                                            return null;
                                          },
                                          onChanged: (inputValue3) {
                                            if(stringValidator.hasMatch(inputValue3)){
                                              setValidator3(true);
                                            } else {
                                              setValidator3(false);
                                            }
                                          },
                                          controller: textController3,
                                          maxLines: 1,
                                          keyboardType: TextInputType.text,
                                          textInputAction: TextInputAction.next,
                                          textCapitalization: TextCapitalization.sentences,
                                          decoration: InputDecoration(
                                              labelText: "Votre proposition...",
                                              hintText: "Votre proposition...",
                                              border: const OutlineInputBorder(),
                                              errorText: isAString3 ? null : "Lettres uniquement"
                                          )
                                      )
                                  ),
                                ),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          children: [
                            const Text("Question 4", style: TextStyle(fontSize: 35.0)),
                            Text(data.question4Label, style: const TextStyle(fontSize: 15.0)),
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                                  child: SizedBox(
                                      width: 200.0,
                                      child: TextFormField(
                                          validator: (inputValue4) {
                                            if(!stringValidator.hasMatch(inputValue4!)){
                                              return "Lettres uniquement";
                                            }
                                            return null;
                                          },
                                          onChanged: (inputValue4) {
                                            if(stringValidator.hasMatch(inputValue4)){
                                              setValidator4(true);
                                            } else {
                                              setValidator4(false);
                                            }
                                          },
                                          controller: textController4,
                                          maxLines: 1,
                                          keyboardType: TextInputType.text,
                                          textInputAction: TextInputAction.send,
                                          textCapitalization: TextCapitalization.sentences,
                                          decoration: InputDecoration(
                                              labelText: "Votre proposition...",
                                              hintText: "Votre proposition...",
                                              border: const OutlineInputBorder(),
                                              errorText: isAString4 ? null : "Lettres uniquement"
                                          )
                                      )
                                  ),
                                ),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Center(
                  child: ElevatedButton(
                    child: Text("Valider mes réponses", style: TextStyle(color: _validationC() ? Colors.white10 : null)),
                    onPressed: () {
                      if(textController1.text != "" && textController2.text != "" && textController3.text != "" && textController4.text != "") {
                        if(_numberFormC.currentState!.validate()){
                          setState(() {
                            isValidForm = true;
                          });
                          updateTest(textController1.text, textController2.text, textController3.text, textController4.text, data);
                        }
                      } else {
                        setState(() {
                          isValidForm = false;
                        });
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Veuillez remplir tous les champs'), backgroundColor: Colors.redAccent,));
                        return;
                      }
                    },
                  ),
                )
              ],
            )
        );
      }
    }

    return Column(
        children: [
          Text(
            '$_counter',
            style: const TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
          ),
          questions
        ]
    );
  }

  updateTest(String reponse1, String reponse2, String reponse3, String reponse4, Test data) async {
    setState(() {
      _timer.cancel();
      _counter = 60;
    });
    int resultat = 0;
    List<String> bonnesReponses = [];
    List<String> reponses = [];
    answer1 = data.question1Answer;
    answer2 = data.question2Answer;
    answer3 = data.question3Answer;
    answer4 = data.question4Answer;

    bonnesReponses.add(answer1);
    bonnesReponses.add(answer2);
    bonnesReponses.add(answer3);
    bonnesReponses.add(answer4);

    reponses.add(reponse1);
    reponses.add(reponse2);
    reponses.add(reponse3);
    reponses.add(reponse4);

    for(var i = 0 ; i <= 3 ; i++) {
      if(reponses[i].contains(bonnesReponses[i])) {
        resultat++;
      }
    }

    final Uri url = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/test_complete_android/${widget.themeLabel},${widget.levelLabel},${widget.testLabel},${widget.testId},${widget.userId},$resultat");

    var response = await http.get(url);

    if(response.statusCode == 200) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Réponses envoyées'), backgroundColor: Colors.lightGreenAccent,));
    } else {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Une erreur s\'est produite lors de l\'envoi de vos réponses'), backgroundColor: Colors.deepOrange,));
   }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${widget.testLabel} - ${widget.levelLabel}"),
      ),
      body: Center(
        child: FutureBuilder<List<Test>>(
            future: fetchQuestions(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return ListView(
                  children: <Widget>[
                    Shimmer.fromColors(
                        baseColor: Colors.deepPurple[300]!,
                        highlightColor: Colors.deepPurpleAccent[100]!,
                        child: Column(
                          children: <int>[0, 1, 2, 3, 4, 5].map((_) => Padding(
                            padding: const EdgeInsets.only(
                                bottom: 10.0,
                                top: 10.0,
                                right: 10.0,
                                left: 10.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        width: 200.0,
                                        height: 35.0,
                                        color: Colors.white,
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 5.0),
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 2.0),
                                      ),
                                      Container(
                                        width: 150.0,
                                        height: 20.0,
                                        color: Colors.white,
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 5.0),
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Container(
                                            width: 10.0,
                                            height: 8.0,
                                            decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Colors.white
                                            ),
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 5.0),
                                          ),
                                          Container(
                                            width: 35.0,
                                            height: 8.0,
                                            color: Colors.white,
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 10.0),
                                          ),
                                          Container(
                                            width: 10.0,
                                            height: 8.0,
                                            decoration: const BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Colors.white
                                            ),
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 5.0),
                                          ),
                                          Container(
                                            width: 35.0,
                                            height: 8.0,
                                            color: Colors.white,
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 10.0),
                                          ),
                                          Container(
                                            width: 10.0,
                                            height: 8.0,
                                            decoration: const BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Colors.white
                                            ),
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 5.0),
                                          ),
                                          Container(
                                            width: 35.0,
                                            height: 8.0,
                                            color: Colors.white,
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 10.0),
                                          ),
                                          Container(
                                            width: 10.0,
                                            height: 8.0,
                                            decoration: const BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Colors.white
                                            ),
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 5.0),
                                          ),
                                          Container(
                                            width: 35.0,
                                            height: 8.0,
                                            color: Colors.white,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )).toList(),
                        ),
                      ),
                  ],
                );
              }
              return ListView(
                  children: snapshot.data!.map((data) => Column(
                    children: [
                      question(context, data, widget.levelLabel),
                    ],
                  )).toList()
              );
            }
        ),
      ),
    );
  }
}