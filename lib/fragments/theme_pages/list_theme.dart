import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:innov_anglais_android/modele/Themee.dart';
import 'package:shimmer/shimmer.dart';

class ThemeeListPage extends StatefulWidget {
  const ThemeeListPage({Key? key}) : super(key: key);

  @override
  _ThemeeListPageState createState() => _ThemeeListPageState();
}

class _ThemeeListPageState extends State<ThemeeListPage> {
  final _formTheme = GlobalKey<FormState>();
  TextEditingController textEditingControllerThemeLabel = TextEditingController();
  bool showInputController = false;

  final Uri url = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/theme_JSON");

  Future<List<Themee>> fetchThemees() async {
    var response = await http.get(url);
    if (response.statusCode == 200) {
      final items = json.decode(response.body).cast<Map<String, dynamic>>();
      List<Themee> listOfThemees = items.map<Themee>((json) {
        return Themee.fromJson(json);
      }).toList();
      return listOfThemees;
    } else {
      throw Exception('Une erreur s\'est produite.');
    }
  }

  void main() {
    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.dumpErrorToConsole(details);
      if (kReleaseMode) exit(1);
    };
  }

  bool _validationTheme() {
    return !(textEditingControllerThemeLabel.text != "");
  }

  insertTheme(String label) async {
    final Uri url = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/insert_theme_android/$label");

    var response = await http.get(url);

    if(response.statusCode == 200) {
      if(response.body.contains("success")) ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Donnée enregistrée'), backgroundColor: Colors.lightGreenAccent,));
      if(response.body.contains("error")) ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Une erreur s\'est produite'), backgroundColor: Colors.deepOrange,));

      /// Actualise la liste et vide les champs
      setState(() {
        textEditingControllerThemeLabel.text = "";
        showInputController = !showInputController;
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Une erreur s\'est produite: error ${response.statusCode}'), backgroundColor: Colors.deepOrange,));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(!showInputController ? 'Innov\'Anglais - Thèmes' : 'Innov\'Anglais - Ajouter Thème'),
        automaticallyImplyLeading: !showInputController ? true : false,
        actions: <Widget>[
          Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    textEditingControllerThemeLabel.text = "";
                    showInputController = !showInputController;
                  });
                },
                child: Icon(
                    !showInputController ? Icons.add_circle : Icons.remove_circle_outlined
                ),
              )
          ),
        ],
      ),
      body: Center(
        child: FutureBuilder<List<Themee>>(
          future: fetchThemees(),
          builder: (context, snapshot) {
            /// Affiche un squelette tant que les données ne sont pas récuperées
            if (!snapshot.hasData) {
              return ListView(
                children: <Widget>[
                  Center(
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey[300]!,
                      highlightColor: Colors.grey[100]!,
                      child: Column(
                        children: <int>[0, 1, 2, 3, 4, 5, 6, 7].map((_) => Padding(
                          padding: const EdgeInsets.only(
                              bottom: 10.0,
                              top: 10.0,
                              right: 10.0,
                              left: 10.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 48.0,
                                height: 48.0,
                                color: Colors.white,
                              ),
                              const Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8.0),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: double.infinity,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 2.0),
                                    ),
                                    Container(
                                      width: double.infinity,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 2.0),
                                    ),
                                    Container(
                                      width: 40.0,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )).toList(),
                      ),
                    ),
                  )
                ],
              );
            }

            /// Si table vide dans la BDD
            if (snapshot.data!.isEmpty) {
              return const Center(
                child: Text("Aucun theme"),
              );
            }

            return showInputController ?
            Form(
                key: _formTheme,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: SizedBox(
                          width: 200.0,
                          child: TextFormField(
                              controller: textEditingControllerThemeLabel,
                              maxLines: 1,
                              onChanged: (value) {
                                setState(() {
                                  _validationTheme();
                                });
                              },
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              textCapitalization: TextCapitalization.sentences,
                              decoration: const InputDecoration(
                                labelText: "Ajouter un thème...",
                                hintText: "Ajouter un thème...",
                                border: OutlineInputBorder(),
                              )
                          )
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: SizedBox(
                          width: 200.0,
                          child: ElevatedButton(
                            child: Text("Ajouter le thème !", style: TextStyle(color: _validationTheme() ? Colors.white10 : null),),
                            onPressed: () {
                              if(textEditingControllerThemeLabel.text != "") {
                                if(_formTheme.currentState!.validate()){
                                  insertTheme(textEditingControllerThemeLabel.text);
                                }
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Veuillez compléter le champ'), backgroundColor: Colors.redAccent,));
                                return;
                              }
                            },
                          )
                      ),
                    )
                  ],
                )
            ) : ListView(
              children: snapshot.data!.map((data) => Card(
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      CupertinoButton(
                        child: ListTile(
                          leading: Text(
                            data.libelleThemee,
                            style: const TextStyle(
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        onPressed: () {

                        },
                      ),
                    ],
                  ),
                ),
              )).toList(),
            );
          },
        ),
      ),
    );
  }
}
