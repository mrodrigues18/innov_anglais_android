import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:innov_anglais_android/modele/Categorie.dart';
import 'package:shimmer/shimmer.dart';

class CategorieListPage extends StatefulWidget {
  const CategorieListPage({Key? key}) : super(key: key);

  @override
  _CategorieListPageState createState() => _CategorieListPageState();
}

class _CategorieListPageState extends State<CategorieListPage> {
  final _formCategory = GlobalKey<FormState>();
  TextEditingController textEditingControllerCategoryLabel = TextEditingController();
  bool showInputController = false;

  final Uri url = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/category_JSON");

  Future<List<Categorie>> fetchCategories() async {
    var response = await http.get(url);

    if (response.statusCode == 200) {
      final items = json.decode(response.body).cast<Map<String, dynamic>>();
      List<Categorie> listOfCategories = items.map<Categorie>((json) {
        return Categorie.fromJson(json);
      }).toList();

      return listOfCategories;
    } else {
      throw Exception('Une erreur s\'est produite.');
    }
  }

  void main() {
    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.dumpErrorToConsole(details);
      if (kReleaseMode) exit(1);
    };
  }

  bool _validationCategory() {
    return !(textEditingControllerCategoryLabel.text != "");
  }

  insertCategory(String label) async {
    final Uri url = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/insert_category_android/$label");

    var response = await http.get(url);

    if(response.statusCode == 200) {
      if(response.body.contains("success")) ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Donnée enregistrée'), backgroundColor: Colors.lightGreenAccent,));
      if(response.body.contains("error")) ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Une erreur s\'est produite'), backgroundColor: Colors.deepOrange,));

      /// Actualise la liste et vide les champs
      setState(() {
        textEditingControllerCategoryLabel.text = "";
        showInputController = !showInputController;
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Une erreur s\'est produite: error ${response.statusCode}'), backgroundColor: Colors.deepOrange,));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(!showInputController ? 'Innov\'Anglais - Catégories' : 'Innov\'Anglais - Ajouter Catégorie'),
        automaticallyImplyLeading: !showInputController ? true : false,
        actions: <Widget>[
          Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    textEditingControllerCategoryLabel.text = "";
                    showInputController = !showInputController;
                  });
                },
                child: Icon(
                    !showInputController ? Icons.add_circle : Icons.remove_circle_outlined
                ),
              )
          ),
        ],
      ),
      body: Center(
        child: FutureBuilder<List<Categorie>>(
          future: fetchCategories(),
          builder: (context, snapshot) {
            /// Affiche un squelette tant que les données ne sont pas récuperées
            if (!snapshot.hasData) {
              return ListView(
                children: <Widget>[
                  Center(
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey[300]!,
                      highlightColor: Colors.grey[100]!,
                      child: Column(
                        children: <int>[0, 1, 2, 3, 4, 5, 6, 7].map((_) => Padding(
                          padding: const EdgeInsets.only(
                              bottom: 10.0,
                              top: 10.0,
                              right: 10.0,
                              left: 10.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 48.0,
                                height: 48.0,
                                color: Colors.white,
                              ),
                              const Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8.0),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: double.infinity,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 2.0),
                                    ),
                                    Container(
                                      width: double.infinity,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 2.0),
                                    ),
                                    Container(
                                      width: 40.0,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )).toList(),
                      ),
                    ),
                  )
                ],
              );
            }

            /// Si table vide dans la BDD
            if (snapshot.data!.isEmpty) {
              return const Center(
                child: Text("Aucune Catégorie"),
              );
            }

            return showInputController ?
            Form(
                key: _formCategory,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: SizedBox(
                          width: 200.0,
                          child: TextFormField(
                              controller: textEditingControllerCategoryLabel,
                              maxLines: 1,
                              onChanged: (value) {
                                setState(() {
                                  _validationCategory();
                                });
                              },
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.done,
                              textCapitalization: TextCapitalization.sentences,
                              decoration: const InputDecoration(
                                labelText: "Ajouter une catégorie...",
                                hintText: "Ajouter une catégorie...",
                                border: OutlineInputBorder(),
                              )
                          )
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: SizedBox(
                          width: 200.0,
                          child: ElevatedButton(
                            child: Text("Ajouter une catégorie !", style: TextStyle(color: _validationCategory() ? Colors.white10 : null),),
                            onPressed: () {
                              if(textEditingControllerCategoryLabel.text != "") {
                                if(_formCategory.currentState!.validate()){
                                  insertCategory(textEditingControllerCategoryLabel.text);
                                }
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Veuillez compléter le champ'), backgroundColor: Colors.redAccent,));
                                return;
                              }
                            },
                          )
                      ),
                    )
                  ],
                )
            ) : ListView(
              children: snapshot.data!.map((data) => Card(
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      CupertinoButton(
                        child: ListTile(
                          title: Text(
                            data.libelleCategorie,
                            style: const TextStyle(
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        onPressed: () {

                        },
                      ),
                    ],
                  ),
                ),
              )).toList(),
            );
          },
        ),
      ),
    );
  }
}