import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart' as http;
import 'package:innov_anglais_android/modele/Firm.dart';
import 'package:innov_anglais_android/modele/parameters.dart';
import 'package:innov_anglais_android/modele/User.dart';
import 'package:shimmer/shimmer.dart';
import 'package:translator/translator.dart';

class AccountViewPage extends StatefulWidget {
  final User currentUser;
  const AccountViewPage({Key? key, required this.currentUser}) : super(key: key);

  @override
  _AccountViewPageState createState() => _AccountViewPageState();
}

class _AccountViewPageState extends State<AccountViewPage> {
  final _formUser = GlobalKey<FormState>();
  TextEditingController textEditingControllerLastName = TextEditingController();
  TextEditingController textEditingControllerFirstName = TextEditingController();
  late String _selected;
  late String _selectedLanguage;

  GoogleTranslator translator = GoogleTranslator();

  var updatedSnackbarSuccess;
  var updatedSnackbarFailed;
  var usernameLabelText;
  var firstNameLabelText;
  var lastNameLabelText;
  var firmLabelText;
  var levelLabelText;

  final Uri urlFirms = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/firms_JSON");
  final Uri urlParameters = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/parameters_JSON");

  Future<List<Firm>> fetchFirms() async {
    var response = await http.get(urlFirms);

    if (response.statusCode == 200) {
      final items = json.decode(response.body).cast<Map<String, dynamic>>();
      List<Firm> listOfFirms = items.map<Firm>((json) {
        return Firm.fromJson(json);
      }).toList();

      return listOfFirms;
    } else {
      throw Exception('Une erreur s\'est produite.');
    }
  }

  Future<List<Parameter>> fetchParameters() async {
    var response = await http.get(urlParameters);

    if (response.statusCode == 200) {
      final items = json.decode(response.body).cast<Map<String, dynamic>>();
      List<Parameter> listOfParameters = items.map<Parameter>((json) {
        return Parameter.fromJson(json);
      }).toList();

      return listOfParameters;
    } else {
      throw Exception('Une erreur s\'est produite.');
    }
  }

  void main() {
    FlutterError.onError = (FlutterErrorDetails details) {
        FlutterError.dumpErrorToConsole(details);
        if(kReleaseMode) exit(1);
      };
  }

  @override
  initState() {
    super.initState();
    _selectedLanguage = widget.currentUser.language_user;
    textEditingControllerLastName.text = widget.currentUser.last_name_user;
    textEditingControllerFirstName.text = widget.currentUser.first_name_user;
    if(widget.currentUser.firmUser != null) _selected = widget.currentUser.firmUser!;
    translate();
  }

  void translate() {
    translator
      .translate("Mise à jour effectuée", to: _selectedLanguage)
      .then((value) {
        setState(() {
          updatedSnackbarSuccess = value.text;
        });
    });
    translator
      .translate("Une erreur s'est produite", to: _selectedLanguage)
      .then((value) {
        setState(() {
          updatedSnackbarFailed = value.text;
        });
    });
    translator
      .translate("Nom d'utilisateur", to: _selectedLanguage)
      .then((value) {
        setState(() {
          usernameLabelText = value.text;
        });
    });
    translator
      .translate("NOM", to: _selectedLanguage)
      .then((value) {
        setState(() {
          lastNameLabelText = value.text;
        });
    });
    translator
      .translate("Prénom", to: _selectedLanguage)
      .then((value) {
        setState(() {
          firstNameLabelText = value.text;
        });
    });
    translator
      .translate("Entreprises", to: _selectedLanguage)
      .then((value) {
        setState(() {
          firmLabelText = value.text;
        });
    });
    translator
      .translate("Niveau", to: _selectedLanguage)
      .then((value) {
        setState(() {
          levelLabelText = value.text;
        });
    });
  }

  void updateUser(String newValue, String idUser) async {
    final Uri urlParameters = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/account_update_android/$newValue/$idUser");
    var response = await http.get(urlParameters);

    if (response.statusCode == 200) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(updatedSnackbarSuccess), backgroundColor: Colors.lightGreenAccent,));
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(updatedSnackbarFailed), backgroundColor: Colors.redAccent,));
    }
  }

  void updateParameters(String newValue, String idUser) async {
    final Uri urlParameters = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/account_update_parameters_android/$newValue/$idUser");
    var response = await http.get(urlParameters);

    if (response.statusCode == 200) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(updatedSnackbarSuccess), backgroundColor: Colors.lightGreenAccent,));
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(updatedSnackbarFailed), backgroundColor: Colors.redAccent,));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Form(
        key: _formUser,
        child: Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: SizedBox(
                      width: 150.0,
                      child: TextField(
                          maxLines: 1,
                          readOnly: true,
                          style: const TextStyle(
                              fontSize: 15.0
                          ),
                          decoration: InputDecoration(
                            labelText: usernameLabelText,
                            hintText: widget.currentUser.usernameUser,
                            border: const OutlineInputBorder(),
                          )
                      ),
                    ),
                  ),
                  FutureBuilder<List<Parameter>>(
                      future: fetchParameters(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return SizedBox(
                            width: 150.0,
                            height: 100.0,
                            child: Shimmer.fromColors(
                              baseColor: Colors.white,
                              highlightColor: Colors.deepPurple,
                              child: ListView.builder(
                                itemBuilder: (_, __) => Padding(
                                  padding: const EdgeInsets.all(25.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: const <Widget>[
                                      SizedBox(
                                        width: 150.0,
                                        child: TextField(
                                            maxLines: 1,
                                            readOnly: true,
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                            decoration: InputDecoration(
                                              enabled: false,
                                              border: OutlineInputBorder(),
                                            )
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                itemCount: 1,
                              ),
                            ),
                          );
                        }

                        return Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: SizedBox(
                            width: 150.0,
                            child: DropdownButtonFormField(
                              value: _selectedLanguage,
                              onChanged: (String? newValue) {
                                setState(() {
                                  _selectedLanguage = newValue!;
                                  translate();
                                });
                                updateParameters(newValue!, widget.currentUser.idUser);
                              },
                              style: const TextStyle(
                                  fontSize: 12.0,
                                  overflow: TextOverflow.fade
                              ),
                              items: snapshot.data!.map<DropdownMenuItem<String>>((Parameter value) {
                                return DropdownMenuItem<String>(
                                  value: value.valueParameter,
                                  child: Row(children: [ Image.network(value.pictureParameter, width: 50.0, height: 50.0, alignment: Alignment.centerLeft,), Text(value.labelParameter)],),
                                );
                              }).toList(),
                            ),
                          ),
                        );
                      }
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: SizedBox(
                      width: 150.0,
                      child: TextField(
                          controller: textEditingControllerLastName,
                          maxLines: 1,
                          keyboardType: TextInputType.name,
                          textCapitalization: TextCapitalization.characters,
                          onChanged: (_) => updateUser(textEditingControllerLastName.text, widget.currentUser.idUser),
                          style: const TextStyle(
                              fontSize: 15.0
                          ),
                          decoration: InputDecoration(
                              labelText: lastNameLabelText,
                              hintText: widget.currentUser.last_name_user,
                              border: const OutlineInputBorder(),
                          )
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: SizedBox(
                      width: 150.0,
                      child: TextField(
                          controller: textEditingControllerFirstName,
                          maxLines: 1,
                          keyboardType: TextInputType.name,
                          textCapitalization: TextCapitalization.sentences,
                          onChanged: (String? newValue) => updateUser(textEditingControllerLastName.text, widget.currentUser.idUser),
                          style: const TextStyle(
                              fontSize: 15.0
                          ),
                          decoration: InputDecoration(
                              labelText: firstNameLabelText,
                              hintText: widget.currentUser.first_name_user,
                              border: const OutlineInputBorder(),
                          )
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FutureBuilder<List<Firm>>(
                      future: fetchFirms(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return SizedBox(
                            width: 150.0,
                            height: 100.0,
                            child: Shimmer.fromColors(
                              baseColor: Colors.white,
                              highlightColor: Colors.deepPurple,
                              child: ListView.builder(
                                itemBuilder: (_, __) => Padding(
                                  padding: const EdgeInsets.all(25.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      SizedBox(
                                        width: 150.0,
                                        child: TextField(
                                            maxLines: 1,
                                            readOnly: true,
                                            style: const TextStyle(
                                              fontSize: 15.0,
                                            ),
                                            decoration: InputDecoration(
                                              enabled: false,
                                              labelText: firmLabelText,
                                              border: const OutlineInputBorder(),
                                            )
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                itemCount: 1,
                              ),
                            ),
                          );
                        }

                        return Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: SizedBox(
                            width: 150.0,
                            child: DropdownButtonFormField(
                              value: _selected,
                              hint: Text(firmLabelText),
                              onChanged: (String? newValue) {
                                setState(() {
                                  _selected = newValue!;
                                  updateUser(newValue, widget.currentUser.idUser);
                                });
                              },
                              style: const TextStyle(
                                  fontSize: 15.0
                              ),
                              items: snapshot.data!.map<DropdownMenuItem<String>>((Firm value) {
                                return DropdownMenuItem<String>(
                                  value: value.nameFirm,
                                  child: Text(value.nameFirm),
                                );
                              }).toList(),
                            ),
                          ),
                        );
                      }
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: SizedBox(
                      width: 150.0,
                      child: TextField(
                          maxLines: 1,
                          readOnly: true,
                          style: const TextStyle(
                              fontSize: 15.0
                          ),
                          decoration: InputDecoration(
                            labelText: levelLabelText,
                            hintText: widget.currentUser.levelUser,
                            border: const OutlineInputBorder(),
                          )
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.network(
                    "http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/parameters/vecteur_parametres.svg",
                    width: 150.0,
                    height: 150.0,
                  )
                ],
              )
            ],
          ),
        ),
      )
    );
  }
}