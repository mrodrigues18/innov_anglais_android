import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter/services.dart';

class PaymentPage extends StatefulWidget {
  const PaymentPage({Key? key}) : super(key: key);

  @override
  _PaymentPageState createState() => _PaymentPageState();
}


class _PaymentPageState extends State<PaymentPage> {
  int _index = 0;
  bool webView = false;
  bool afficherTexteCopier = false;
  Widget maWebView = Container();

  _payer(String amount, String url) {
    setState(() {
      maWebView = Scaffold(
        appBar: AppBar(
          leading: GestureDetector(child: const Icon(Icons.arrow_back), onTap: () => setState(() { webView = false; maWebView = Container(); })),
          title: Text("Abonnement à " + amount + "€/mois"),
        ),
        body: WebView(
            javascriptMode: JavascriptMode.unrestricted,
            initialUrl: url
        ),
      );
    });
  }

  displayCopyText() {
    Future.delayed(const Duration(seconds: 5), () {
      setState(() {
        afficherTexteCopier = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return webView ? maWebView : Column(
      children: [
        SizedBox(
            height: 200, // card height
            child: GestureDetector(
              child: PageView.builder(
                itemCount: 3,
                controller: PageController(viewportFraction: 0.7),
                onPageChanged: (int index) => setState(() => _index = index),
                itemBuilder: (_, i) {
                  var j=i;
                  var k=i;
                  k+=1;
                  j = 5 * (j + 1);
                  return Transform.scale(
                    scale: i == _index ? 1 : 0.9,
                    child: Card(
                      elevation: 10,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Abonnement ${i+1}",
                                style: const TextStyle(fontSize: 30),
                              ),
                            ],
                          ),
                          const Divider(color: Colors.blueGrey, thickness: 1.5,),
                          Row(
                            children: const [
                              Padding(padding: EdgeInsets.only(left: 5.0, right: 10.0), child: Icon(Icons.check, color: Colors.green,)),
                              Text("Débloquez un test bonus"),
                            ],
                          ),
                          Row(
                            children: [
                              Padding(padding: const EdgeInsets.only(left: 5.0, right: 10.0), child: k > 1 ? const Icon(Icons.check, color: Colors.green) : const Icon(Icons.cancel, color: Colors.red,)),
                              const Text("Débloquez deux tests bonus"),
                            ],
                          ),
                          Row(
                            children: [
                              Padding(padding: const EdgeInsets.only(left: 5.0, right: 10.0), child: k > 2 ? const Icon(Icons.check, color: Colors.green) : const Icon(Icons.cancel, color: Colors.red,)),
                              const Text("Débloquez trois tests bonus"),
                            ],
                          ),
                          const Divider(color: Colors.blueGrey, thickness: 1.5,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ElevatedButton(
                                onPressed: () {
                                  var lien = "";
                                  switch(j) {
                                    case 5:
                                      lien = "https://buy.stripe.com/test_3cs17VaDm4lD2ty5kk";
                                      break;
                                    case 10:
                                      lien = "https://buy.stripe.com/test_dR6cQDfXG6tL6JO6op";
                                      break;
                                    case 15:
                                      lien = "https://buy.stripe.com/test_00geYL26QcS95FK4gi";
                                      break;
                                  }
                                  _payer("$j", lien);
                                  setState(() {
                                    webView = true;
                                  });
                                },
                                child: Text("Payer $j€", style: const TextStyle(fontSize: 25.0),),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  );
                },
              ),
              onTap: () {

              },
            )
        ),
        GestureDetector(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              !afficherTexteCopier ? const Text("Code promo -20%:") : Container(),
              !afficherTexteCopier ? const Padding(padding: EdgeInsets.only(left: 2.0, right: 2.0), child: Icon(Icons.copy, size: 20.0,)) : Container(),
              afficherTexteCopier ? const Text("Texte copié", style: TextStyle(color: Colors.lightGreen),) : Text("BTSINFO")
            ],
          ),
          onTap: () {
              setState(() {
                afficherTexteCopier = true;
              });
              displayCopyText();
            Clipboard.setData(const ClipboardData(text: "BTSINFO"));
          },
        ),
        Padding(
          padding: const EdgeInsets.only(top: 100.0),
          child: Image.network("http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/vecteur_payer.jpg", width: 300,),
        )
      ],
    );
  }
}