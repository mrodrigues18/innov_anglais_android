import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:innov_anglais_android/modele/User.dart';
import 'package:innov_anglais_android/pages/home_page.dart';
import 'package:shimmer/shimmer.dart';

class UserListPage extends StatefulWidget {
  const UserListPage({Key? key}) : super(key: key);

  @override
  _UserListPageState createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  final Uri url = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/users_JSON");

  Future<List<User>> fetchUsers() async {
    var response = await http.get(url);
    if (response.statusCode == 200) {
      final items = json.decode(response.body).cast<Map<String, dynamic>>();
      List<User> listOfUsers = items.map<User>((json) {
        return User.fromJson(json);
      }).toList();

      return listOfUsers;
    } else {
      throw Exception('Une erreur s\'est produite.');
    }
  }

  void main() {
    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.dumpErrorToConsole(details);
      if (kReleaseMode) exit(1);
    };
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Innov\'Anglais - Utilisateurs')),
      body: Center(
        child: FutureBuilder<List<User>>(
          future: fetchUsers(),
          builder: (context, snapshot) {
            /// Affiche un squelette tant que les données ne sont pas récuperées
            if (!snapshot.hasData) {
              return ListView(
                children: <Widget>[
                  Center(
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey[300]!,
                      highlightColor: Colors.grey[100]!,
                      child: Column(
                        children: <int>[0, 1, 2, 3, 4, 5, 6, 7].map((_) => Padding(
                          padding: const EdgeInsets.only(
                              bottom: 10.0,
                              top: 10.0,
                              right: 10.0,
                              left: 10.0),
                          child: Row(
                            crossAxisAlignment:
                                CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 48.0,
                                height: 48.0,
                                color: Colors.white,
                              ),
                              const Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8.0),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: double.infinity,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 2.0),
                                    ),
                                    Container(
                                      width: double.infinity,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 2.0),
                                    ),
                                    Container(
                                      width: 40.0,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )).toList(),
                      ),
                    ),
                  )
                ],
              );
            }

            /// Si table vide dans la BDD
            if (snapshot.data!.isEmpty) {
              return const Center(
                child: Text("Aucun utilisateur"),
              );
            }

            return ListView(
              children: snapshot.data!.map((data) => Card(
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      OutlinedButton(
                        child: ListTile(
                          leading: Text(
                            data.usernameUser,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          title: (data.libellePaiement == "Pas de paiement" && data.roleUser=="[\"ROLE_ADMIN\"]")
                              ? Text("Utilisateur : "'\n'
                                     "Nom : " + data.first_name_user + '\n'
                                     "Prénom : " + data.last_name_user + '\n'
                                     "Rôle : " + "Administrateur" + '\n'
                                     "Âge : " + data.ageUser + '\n'
                                     "Niveau : " + data.levelUser + '\n'
                                     "Entreprise : " + data.firmUser!)
                              : Text(
                                  data.usernameUser +
                                  "\n" +
                                  data.first_name_user +
                                  "\n" +
                                  data.last_name_user +
                                  "\n" +
                                  data.roleUser +
                                  "\n" +
                                  data.last_name_user +
                                  "\n" +
                                  data.ageUser +
                                  "\n" +
                                  data.levelUser +
                                  "\n" +
                                  data.libellePaiement! +
                                  "\n" +
                                  data.firmUser!, softWrap: true,),
                        ),
                        onLongPress: () {
                          showDialog(
                            context: context,
                            builder: (context) {
                             return AlertDialog(
                               title: const Text("Se connecter en tant que..."),
                               content: Text("Souhaitez-vous vous connecter en tant que '${data.usernameUser}'"),
                               actions: [
                                 OutlinedButton(
                                   onPressed: () {
                                     Navigator.of(context).pop();
                                   },
                                   child: const Text(
                                     "Annuler",
                                     style: TextStyle(color: Colors.redAccent),
                                   )
                                 ),
                                 OutlinedButton(
                                   onPressed: () {
                                     Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomePage(currentUser: data)));
                                   },
                                   child: const Text(
                                     "Se connecter",
                                     style: TextStyle(color: Colors.greenAccent),
                                   )
                                 ),
                               ],
                             );
                            }
                          );
                        },
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                title: const Text("Vue utilisateur"),
                                content: Text(
                                  "Identifiant: " + data.usernameUser +
                                  '\nNom: ' +
                                  data.first_name_user +
                                  '\nPrénom: ' +
                                  data.last_name_user +
                                  '\nRôle: ' +
                                  data.roleUser +
                                  "\nÂge: " +
                                  data.ageUser +
                                  "\nNiveau: " +
                                  data.levelUser +
                                  "\nEntreprise: " +
                                  data.firmUser!
                                ),
                              );
                            },
                          );
                        },
                      ),
                    ],
                  ),
                ),
              )).toList(),
            );
          },
        ),
      ),
    );
  }
}
