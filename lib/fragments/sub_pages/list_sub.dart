import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:innov_anglais_android/modele/Subscription.dart';
import 'package:shimmer/shimmer.dart';

class SubscriptionListPage extends StatefulWidget {
  const SubscriptionListPage({Key? key}) : super(key: key);

  @override
  _SubscriptionListPageState createState() => _SubscriptionListPageState();
}

class _SubscriptionListPageState extends State<SubscriptionListPage> {
  final _formSub = GlobalKey<FormState>();
  TextEditingController textEditingControllerSubscriptionLabel = TextEditingController();
  TextEditingController textEditingControllerSubscriptionAmount = TextEditingController();
  bool showInputController = false;

  final Uri url = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/subscription_JSON");

  Future<List<Subscription>> fetchSubscriptions() async {
    var response = await http.get(url);

    if (response.statusCode == 200) {
      final items = json.decode(response.body).cast<Map<String, dynamic>>();
      List<Subscription> listOfSubscriptions = items.map<Subscription>((json) {
        return Subscription.fromJson(json);
      }).toList();

      return listOfSubscriptions;
    } else {
      throw Exception('Une erreur s\'est produite.');
    }
  }

  void main() {
    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.dumpErrorToConsole(details);
      if (kReleaseMode) exit(1);
    };
  }

  bool _validationSub() {
    return !(textEditingControllerSubscriptionLabel.text != "" && textEditingControllerSubscriptionAmount.text != "");
  }

  insertSubscription(String label, String number) async {
    final Uri url = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/insert_subscription_android/$label/$number");

    var response = await http.get(url);

    if(response.statusCode == 200) {
      if(response.body.contains("success")) ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Donnée enregistrée'), backgroundColor: Colors.lightGreenAccent,));
      if(response.body.contains("error")) ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Une erreur s\'est produite'), backgroundColor: Colors.deepOrange,));

      /// Actualise la liste et vide les champs
      setState(() {
        textEditingControllerSubscriptionLabel.text = "";
        textEditingControllerSubscriptionAmount.text = "";
        showInputController = !showInputController;
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Une erreur s\'est produite: error ${response.statusCode}'), backgroundColor: Colors.deepOrange,));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(!showInputController ? 'Innov\'Anglais - Abonnements' : 'Innov\'Anglais - Ajouter Abonnement'),
        automaticallyImplyLeading: !showInputController ? true : false,
        actions: <Widget>[
          Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    textEditingControllerSubscriptionLabel.text = "";
                    textEditingControllerSubscriptionAmount.text = "";
                    showInputController = !showInputController;
                  });
                },
                child: Icon(
                    !showInputController ? Icons.add_circle : Icons.remove_circle_outlined
                ),
              )
          ),
        ],
      ),
      body: Center(
        child: FutureBuilder<List<Subscription>>(
          future: fetchSubscriptions(),
          builder: (context, snapshot) {
            /// Affiche un squelette tant que les données ne sont pas récuperées
            if (!snapshot.hasData) {
              return ListView(
                children: <Widget>[
                  Center(
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey[300]!,
                      highlightColor: Colors.grey[100]!,
                      child: Column(
                        children: <int>[0, 1, 2, 3, 4, 5, 6, 7].map((_) => Padding(
                          padding: const EdgeInsets.only(
                              bottom: 10.0,
                              top: 10.0,
                              right: 10.0,
                              left: 10.0),
                          child: Row(
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 48.0,
                                height: 48.0,
                                color: Colors.white,
                              ),
                              const Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8.0),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: double.infinity,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 2.0),
                                    ),
                                    Container(
                                      width: double.infinity,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 2.0),
                                    ),
                                    Container(
                                      width: 40.0,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )).toList(),
                      ),
                    ),
                  )
                ],
              );
            }

            /// Si table vide dans la BDD
            if (snapshot.data!.isEmpty) {
              return const Center(
                child: Text("Aucun abonnement"),
              );
            }

            return showInputController ?
            Form(
                key: _formSub,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: SizedBox(
                          width: 200.0,
                          child: TextFormField(
                              controller: textEditingControllerSubscriptionLabel,
                              maxLines: 1,
                              onChanged: (value) {
                                setState(() {
                                  _validationSub();
                                });
                              },
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              textCapitalization: TextCapitalization.sentences,
                              decoration: const InputDecoration(
                                labelText: "Ajouter un type d'abonnement...",
                                hintText: "Ajouter un type d'abonnement...",
                                border: OutlineInputBorder(),
                              )
                          )
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: SizedBox(
                          width: 200.0,
                          child: TextFormField(
                              controller: textEditingControllerSubscriptionAmount,
                              maxLines: 1,
                              onChanged: (value) {
                                setState(() {
                                  _validationSub();
                                });
                              },
                              keyboardType: TextInputType.number,
                              textInputAction: TextInputAction.done,
                              decoration: const InputDecoration(
                                labelText: "Nombre de mois...",
                                hintText: "Nombre de mois...",
                                border: OutlineInputBorder(),
                              )
                          )
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: SizedBox(
                          width: 200.0,
                          child: ElevatedButton(
                            child: Text("Ajouter l'abonnement !", style: TextStyle(color: _validationSub() ? Colors.white10 : null),),
                            onPressed: () {
                              if(textEditingControllerSubscriptionLabel.text != "" && textEditingControllerSubscriptionAmount.text != "") {
                                if(_formSub.currentState!.validate()){
                                  insertSubscription(textEditingControllerSubscriptionLabel.text, textEditingControllerSubscriptionAmount.text);
                                }
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Veuillez compléter le champ'), backgroundColor: Colors.redAccent,));
                                return;
                              }
                            },
                          )
                      ),
                    )
                  ],
                )
            ) : ListView(
              children: snapshot.data!.map((data) => Card(
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      CupertinoButton(
                        child: ListTile(
                          leading: Text(
                            data.libelleSubscription,
                            style: const TextStyle(
                                fontWeight: FontWeight.bold),
                          ),
                          title: Text("Renouvellement : " + data.nb_foisSubscription + " mois"),
                        ),
                        onPressed: () {

                        },
                      ),
                    ],
                  ),
                ),
              )).toList(),
            );
          },
        ),
      ),
    );
  }
}