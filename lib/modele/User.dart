class User {
  final String idUser;
  final String usernameUser;
  final String roleUser;
  final String last_name_user;
  final String first_name_user;
  late final String? firmUser;
  final String ageUser;
  final String levelUser;
  final String? libellePaiement;
  final String email;
  late final String language_user;

  User({
    required this.idUser,
    required this.usernameUser,
    required this.roleUser,
    required this.last_name_user,
    required this.first_name_user,
    this.firmUser,
    required this.ageUser,
    required this.levelUser,
    this.libellePaiement,
    required this.email,
    required this.language_user
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      idUser: json['idUser'],
      usernameUser: json['usernameUser'],
      roleUser: json['roleUser'],
      last_name_user: json['last_name_user'],
      first_name_user: json['first_name_user'],
      firmUser: json['firmUser'],
      ageUser: json['ageUser'],
      levelUser: json['levelUser'],
      libellePaiement: json['libellePaiement'],
      email: json['email'],
      language_user: json['language']
    );
  }
}