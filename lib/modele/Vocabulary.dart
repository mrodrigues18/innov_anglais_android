class Vocabulary {
  String idVocabulary;
  String libelleVocabulary;

  Vocabulary({required this.idVocabulary, required this.libelleVocabulary});

  factory Vocabulary.fromJson(Map<String, dynamic> json) {
    return Vocabulary(idVocabulary: json['idVocabulary'], libelleVocabulary: json['libelleVocabulary']);
  }
}
