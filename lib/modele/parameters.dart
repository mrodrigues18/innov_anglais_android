class Parameter {
  String parameterId;
  String pictureParameter;
  String labelParameter;
  String valueParameter;

  Parameter({required this.parameterId, required this.pictureParameter, required this.labelParameter, required this.valueParameter});

  factory Parameter.fromJson(Map<String, dynamic> json) {
    return Parameter(
        parameterId: json['parameter_id'],
        pictureParameter: json['picture_parameter'],
        labelParameter: json['label_parameter'],
        valueParameter: json['value_parameter']
    );
  }
}