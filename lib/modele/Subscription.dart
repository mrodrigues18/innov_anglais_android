class Subscription {
  String idSubscription;
  String libelleSubscription;
  String nb_foisSubscription;

  Subscription({required this.idSubscription, required this.libelleSubscription, required this.nb_foisSubscription});

  factory Subscription.fromJson(Map<String, dynamic> json) {
    return Subscription(
        idSubscription: json['idSubscription'],
        libelleSubscription: json['libelleSubscription'],
        nb_foisSubscription: json['nb_foisSubscription']

    );
  }
}