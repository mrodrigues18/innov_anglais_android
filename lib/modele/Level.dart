class Level {
  String idLevel;
  String libelleLevel;

  Level({required this.idLevel, required this.libelleLevel});

  factory Level.fromJson(Map<String, dynamic> json) {
    return Level(
        idLevel: json['idLevel'],
        libelleLevel: json['libelleLevel']
    );
  }
}