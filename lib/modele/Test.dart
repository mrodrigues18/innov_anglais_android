class Test {
  String testId,
    testLabel,
    levelId,
    levelLabel,
    themeLabel,
    question1Label,
    question1FakeWords1,
    question1FakeWords2,
    question1Answer,
    question2Label,
    question2FakeWords1,
    question2FakeWords2,
    question2Answer,
    question3Label,
    question3FakeWords1,
    question3FakeWords2,
    question3Answer,
    question4Label,
    question4FakeWords1,
    question4FakeWords2,
    question4Answer;

  Test({
    required this.testId,
    required this.testLabel,
    required this.levelId,
    required this.levelLabel,
    required this.themeLabel,
    required this.question1Label,
    required this.question1FakeWords1,
    required this.question1FakeWords2,
    required this.question1Answer,
    required this.question2Label,
    required this.question2FakeWords1,
    required this.question2FakeWords2,
    required this.question2Answer,
    required this.question3Label,
    required this.question3FakeWords1,
    required this.question3FakeWords2,
    required this.question3Answer,
    required this.question4Label,
    required this.question4FakeWords1,
    required this.question4FakeWords2,
    required this.question4Answer
  });

  factory Test.fromJson(Map<String, dynamic> json) {
    return Test(
        testId:               json['test_id'],
        testLabel:            json['test_label'],
        levelId:              json['level_id'],
        levelLabel:           json['level_label'],
        themeLabel:           json['theme_label'],
        question1Label:       json['question_1_label'],
        question1FakeWords1:  json['question_1_fake_words_1'],
        question1FakeWords2:  json['question_1_fake_words_2'],
        question1Answer:      json['question_1_answer'],
        question2Label:       json['question_2_label'],
        question2FakeWords1:  json['question_2_fake_words_1'],
        question2FakeWords2:  json['question_2_fake_words_2'],
        question2Answer:      json['question_2_answer'],
        question3Label:       json['question_3_label'],
        question3FakeWords1:  json['question_3_fake_words_1'],
        question3FakeWords2:  json['question_3_fake_words_2'],
        question3Answer:      json['question_3_answer'],
        question4Label:       json['question_4_label'],
        question4FakeWords1:  json['question_4_fake_words_1'],
        question4FakeWords2:  json['question_4_fake_words_2'],
        question4Answer:      json['question_4_answer']
    );
  }
}

class TestDo {
  String testLabel,
      testId,
      levelLabel,
      themeLabel;

  TestDo({
    required this.testLabel,
    required this.testId,
    required this.levelLabel,
    required this.themeLabel,
  });

  factory TestDo.fromJson(Map<String, dynamic> json) {
    return TestDo(
      testLabel:  json['test_label'],
      testId:     json['test_id'],
      levelLabel: json['level_label'],
      themeLabel: json['theme_label']
    );
  }
}