import 'dart:io';
import 'package:innov_anglais_android/pages/sign_in_page.dart';
import 'package:flutter/material.dart';

void main() async {
  HttpOverrides.global = MyHttpOverrides();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Innov\'Anglais',
      theme: ThemeData(
          primaryColor: Colors.deepPurpleAccent,
          brightness: Brightness.light,
          bottomAppBarColor: Colors.deepPurpleAccent,
          colorScheme: const ColorScheme.light(secondaryVariant: Colors.deepPurple)
      ),
      darkTheme: ThemeData(
        primaryColor: Colors.deepPurple,
        bottomAppBarColor: Colors.deepPurple,
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.deepPurple, brightness: Brightness.dark).copyWith(secondary: Colors.deepPurpleAccent),
      ),
      home: const SignInPage(),
    );
  }
}

class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    // TODO: implement createHttpClient
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}
